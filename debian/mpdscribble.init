#! /bin/sh
### BEGIN INIT INFO
# Provides:          mpdscribble
# Required-Start:    $local_fs $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Should-Start:      mpd
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: mpdscribble initscript
# Description:       This script starts Music Player Daemon Last.fm Client as daemon.
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/mpdscribble
NAME=mpdscribble
DESC="Music Player Daemon Last.fm Client"
PIDFILE=/var/run/$NAME.pid

# user which will run this daemon
USER=mpdscribble
GROUP=mpdscribble

test -x $DAEMON || exit 0

# Include mpdscribble defaults if available
if [ -f /etc/default/mpdscribble ] ; then
    . /etc/default/mpdscribble
fi

. /lib/lsb/init-functions

case "$1" in
  start)
    if [ ! "x$MPD_SYSTEMWIDE" = x1 ]; then
        log_warning_msg "Not starting $DESC, disabled via /etc/default/mpdscribble"
        exit 0
    fi
    log_daemon_msg "Starting $DESC" $NAME
    start-stop-daemon  --start --quiet \
        --pidfile $PIDFILE --background --exec $DAEMON -- \
        --daemon-user $USER --pidfile $PIDFILE $DAEMON_OPTS
    log_end_msg $?
    ;;
  stop)
    log_daemon_msg "Stopping $DESC" $NAME
    start-stop-daemon --stop --quiet --pidfile $PIDFILE \
        --exec $DAEMON
    RET=$?
    rm -f $PIDFILE
    log_end_msg $RET
    ;;
  restart|force-reload)
    sh $0 stop
    sleep 1
    sh $0 start
    ;;
  status)
    status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
    ;;
  *)
    N=/etc/init.d/$NAME
    echo "Usage: $N {start|stop|restart|force-reload}" >&2
    exit 1
    ;;
esac

exit 0
