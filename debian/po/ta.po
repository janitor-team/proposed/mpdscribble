# translation of ta.po to TAMIL
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Dr.T.Vasudevan <agnihot3@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ta\n"
"Report-Msgid-Bugs-To: mpdscribble@packages.debian.org\n"
"POT-Creation-Date: 2008-09-22 14:51+0200\n"
"PO-Revision-Date: 2007-03-10 12:33+0530\n"
"Last-Translator: Dr.T.Vasudevan <agnihot3@gmail.com>\n"
"Language-Team: TAMIL <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: boolean
#. Description
#: ../mpdscribble.templates:1001
msgid "Install system mpdscribble service?"
msgstr "எம்பிடிஸ்க்ரிபில் சேவையை நிறுவலாமா?"

#. Type: boolean
#. Description
#: ../mpdscribble.templates:1001
msgid ""
"You can install mpdscribble as a system daemon.  The mpdscribble service "
"will be started on boot. Note that is not necessary to run mpd as a system "
"service as it runs fine when started manually using a regular user account."
msgstr ""
"எம்பிடிஸ்க்ரிபில் ஐ கணினி கிங்கரனாக நிறுவலாம். கணினியை துவக்கும்போது இதுவும் துவக்கப் "
"படும். இதை கணினி சேவையாக இயக்கத் தேவையில்லை.வழக்கமான பயனர் கணக்கில் கைமுறையாக "
"துவக்கினாலும் நன்றாகவே இயங்கும். "

#. Type: string
#. Description
#: ../mpdscribble.templates:2001
msgid "Last.fm username:"
msgstr "ஆடியோ ஸ்க்ராப்லர் பயனர் பெயர்:"

#. Type: string
#. Description
#: ../mpdscribble.templates:2001
msgid "Enter username you use on Last.fm."
msgstr "ஆடியோ ஸ்க்ராப்லர் க்கு பயன்படுத்தும் பயனர் பெயரை உள்ளிடுக."

#. Type: password
#. Description
#: ../mpdscribble.templates:3001
msgid "Last.fm password:"
msgstr "ஆடியோ ஸ்க்ராப்லர் கடவுச்சொல்:"

#. Type: password
#. Description
#: ../mpdscribble.templates:3001
msgid "Enter password you use on Last.fm."
msgstr "ஆடியோ ஸ்க்ராப்லர் க்கு பயன்படுத்தும் கடவுச்சொல்லை உள்ளிடுக."
